Ionic Firebase Based App
========================

A project based on Ionic, Cordova, Angular and Firebase that optionally supports using custom SCSS.

## Using this project

To run this project, go the the desired location from your terminal.

* clone from the repository: 
```bash
$ git clone https://suhailtaj@bitbucket.org/suhailtaj/grocerychecklist.git
```

* Globally install cordova and ionic frame works.
```bash
$ npm install -g cordova ionic
```
* Then run:
```bash
$ cd grocerychecklist/
$ sudo npm install
$ ionic serve
```

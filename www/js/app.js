// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic', 'firebase'])

.factory('Items', ['$firebaseArray', function($firebaseArray) {
    var itemsRef = new Firebase('https://grocerysuhail.firebaseio.com/');
    return $firebaseArray(itemsRef);
}])

.controller('ListCtrl', function($scope, $ionicListDelegate, Items) {

    $scope.items = Items;

    $scope.addItem = function() {
        var name = prompt('What do you need to buy?');
        if (name) {
            $scope.items.$add({
                'name': name
            });
        }
    };

    $scope.deleteList = function() {
        
        var fredRef = new Firebase(' https://grocerysuhail.firebaseio.com/');
        var onComplete = function(error) {
            if (error) {
                console.log('Synchronization failed');
            } else {
                console.log('Synchronization succeeded');
            }
        };
        fredRef.remove(onComplete);

    }

    $scope.purchaseItem = function(item) {
        var itemRef = new Firebase('https://grocerysuhail.firebaseio.com/' + item.$id);
        itemRef.child('status').set('purchased');
        $ionicListDelegate.closeOptionButtons();
    };
});
